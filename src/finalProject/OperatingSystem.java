package finalProject;


public class OperatingSystem {
	 private int id;
	    private String name;
	    private String developer;
	    private String osFamily;
	    private String latestVersion;
	    private String delete;
	 
//	    public OperatingSystem() {
	        
//	    }
	 
//	    public OperatingSystem(int id, String name, String developer, String osFamily, String latestVersion, String delete) {
//	        this.id = id;
//	        this.name = name;
//	        this.developer = developer;
//	        this.osFamily = osFamily;
//	        this.latestVersion = latestVersion;
//	        this.delete = delete;
//	    }
	 
	 
	    public String getDelete() {
	        return delete;
	    }
	 
	 
	    public void setDelete(String delete) {
	        this.delete = delete;
	    }
	 
	 
	    public int getId() {
	        return id;
	    }
	 
	    public void setId(int id) {
	        this.id = id;
	    }
	 
	    public String getName() {
	        return name;
	    }
	 
	    public void setName(String name) {
	        this.name = name;
	    }
	 
	    public String getDeveloper() {
	        return developer;
	    }
	 
	    public void setDeveloper(String developer) {
	        this.developer = developer;
	    }
	 
	    public String getOsFamily() {
	        return osFamily;
	    }
	 
	    public void setOsFamily(String osFamily) {
	        this.osFamily = osFamily;
	    }
	 
	    public String getLatestVersion() {
	        return latestVersion;
	    }
	 
	    public void setLatestVersion(String latestVersion) {
	        this.latestVersion = latestVersion;
	    }
	 
	    @Override
	    public String toString() {
	        return id + " " + name+ " " + developer + " " + osFamily + " " + latestVersion;
	    }

}
