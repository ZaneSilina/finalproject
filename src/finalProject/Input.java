package openFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Input {

	private String filePath;
	private String fileExtension;

	public void process() {

		try {
			this.filePath = this.getFromConsole();
			this.setFileExt();

		} catch (IOException | IncorrectFileExt e) {
			e.printStackTrace();
		}
	}
	
	public String processNew() {
		try {
			this.fileExtension = this.getFileExtentionFromConsole();
			this.checkFileExtension(fileExtension);
		} catch (IOException | IncorrectFileExt e) {
			e.printStackTrace();
			processNew();
		}
		return fileExtension;
	}

	private String getFromConsole() throws IOException {
		System.out.println("Enter the filepath:");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		return reader.readLine();
	}

	private void setFileExt() throws IncorrectFileExt {
		this.filePath = this.filePath.toLowerCase();
		StringBuffer fileExt = new StringBuffer("");

		for (int i = this.filePath.length() - 1; i > 0; i--) {
			if (this.filePath.charAt(i) == '.')
				break;
			fileExt.append(this.filePath.charAt(i));
		}

		String fileExtString = fileExt.reverse().toString();
		this.checkFileExtension(fileExtString);
		this.fileExtension = fileExtString;
	}

	private void checkFileExtension(String ext) throws IncorrectFileExt {

		if (!ext.equals("json") && !ext.equals("xml") && !ext.equals("csv"))
			throw new IncorrectFileExt(ext);

	}

	public String getFilePath() {
		return this.filePath;
	}

	public String getFileExtension() {
		return this.fileExtension;
	}
	
	 
    public String getIDFromConsole() throws IOException {
        System.out.println("Enter ID to find:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
    
    public String getNameFromConsole() throws IOException {
        System.out.println("Enter Name to find:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
    
    public String getUpdateFromConsole() throws IOException {
        System.out.println("Enter latest version to update:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
    
    public String getFileExtentionFromConsole() throws IOException {
        System.out.println("Enter file format:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
    
    public String getIDToDelete() throws IOException {
        System.out.println("Enter OS ID to delete:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
   
    public String getCommandFromUser() throws IOException {
        System.out.println("What would you like to do? Choose option: find, update, delete, read:");
        System.out.println("Input 'exit' to finish");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }

}

class IncorrectFileExt extends Exception {

	String ext;

	private static final long serialVersionUID = 1L;

	public IncorrectFileExt(String ext) {
		this.ext = ext;
	}

	@Override
	public void printStackTrace() {
		System.err.println("Extension " + this.ext + " cannot be used");
	}

}
