package finalProject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.function.Predicate;



public class CSV extends FileType {

    private static String delimiter;
    
    public static void setDelimiter(String delimiter) {
        CSV.delimiter = delimiter;
    }
    
 
    @Override
    public void populateData(String content) throws Exception {
        this.operatingSystems = new ArrayList<OperatingSystem>();
        String[] lines = content.split(System.lineSeparator());
 
        for (int i = 1; i < lines.length; i++) {
            StringTokenizer columnNames = new StringTokenizer(lines[0], CSV.delimiter);
            StringTokenizer lineValues = new StringTokenizer(lines[i], CSV.delimiter);
            OperatingSystem operatingSystem = new OperatingSystem();
            this.operatingSystems.add(operatingSystem);
            
        int numberOfColumns = lineValues.countTokens();
            
            for (int j = 0; j < numberOfColumns; j++) {
 
                switch ((String) columnNames.nextElement()) {
                case "ID":
                    operatingSystem.setId(Integer.valueOf((String) lineValues.nextElement()));
                    break;
                case "Name":
                    operatingSystem.setName((String) lineValues.nextElement());
                    break;
                case "Developer":
                    operatingSystem.setDeveloper((String) lineValues.nextElement());
                    break;
                case "OSFamily":
                    operatingSystem.setOsFamily((String) lineValues.nextElement());
                    break;
                case "LatestVersion":
                    operatingSystem.setLatestVersion((String) lineValues.nextElement());
                    break;
                case "Delete":
                    operatingSystem.setDelete((String) lineValues.nextElement());
                    break;
 
                }
            }
 
            }
        }
    
    @Override
    public void populateFile(List <String> list) throws Exception {
    	if (!(list.isEmpty())) {
    	String setDelimiter = list.toString().replace(":", ";");
        String removeFirstBracket = setDelimiter.replace("[", "");
        String writeToFile = removeFirstBracket.replace("]", ";");
        
        String csvFile = "C:\\Users\\Dima\\eclipse-workspace\\finalproject\\src\\finalProject\\newCSV.csv";
        FileWriter writer = new FileWriter(csvFile);
        String header = "ID;Name;Developer;OSFamily;LatestVersion;\n";
        writer.write(header);
        writer.write(writeToFile);
        
        writer.flush();
        writer.close();
        
    	}
    }
	
}
