package finalProject;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;



public class XML extends FileType {

    @Override
    public void populateData(String content) throws Exception {
        Document xmlDocument = this.convertToXMLDocument(content);
        this.processXMLDocument(xmlDocument);
    }
 
    private Document convertToXMLDocument(String xmlString) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlString)));
    }
 
    private void processXMLDocument(Document xmlDocument) {
        Element OperatingSystems = xmlDocument.getDocumentElement();
        this.operatingSystems = new ArrayList<OperatingSystem>();
 
        NodeList operatingSystemsElements = OperatingSystems.getElementsByTagName("OS");
        for (int i = 0; i < operatingSystemsElements.getLength(); i++) {
            Element operatingSystemElement = (Element) operatingSystemsElements.item(i);
            NodeList operatingSystemParameters = operatingSystemElement.getChildNodes();
 
            OperatingSystem operatingSystem = new OperatingSystem();
            this.operatingSystems.add(operatingSystem);
            
            operatingSystem.setId(Integer.parseInt(operatingSystemElement.getAttribute("ID")));
 
            for (int j = 0; j < operatingSystemParameters.getLength(); j++) {
 
                Node parameter =  operatingSystemParameters.item(j);
 
                switch (parameter.getNodeName()) {
             
                case "Name": {
                    operatingSystem.setName(parameter.getTextContent());
                    break;
                }
                case "Developer": {
                    operatingSystem.setDeveloper(parameter.getTextContent());
                    break;
                }
                case "OSFamily": {
                    operatingSystem.setOsFamily(parameter.getTextContent());
                    break;
                }
                case "LatestVersion": {
                    operatingSystem.setLatestVersion(parameter.getTextContent());
                    break;
                }
                case "Delete": {
                    operatingSystem.setDelete(parameter.getTextContent());
                    if (operatingSystem.getDelete().isEmpty())
                        operatingSystem.setDelete(null);
                    break;
                }
                }
            }
        }
      }
    
    @Override
    public void populateFile(List <String> list) throws Exception {
    	if (!list.isEmpty()) {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        String replaceCole = list.toString().replace(":",";");
        String removeFirstBracket = replaceCole.replace("[", "");
        String writeToFile = removeFirstBracket.replace("]", "");
        String [] getData = writeToFile.split(";");
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        
        Element root = doc.createElement("OperatingSystems");
        doc.appendChild(root);
        
        Element operatingS = doc.createElement("OS");
        root.appendChild(operatingS);
        
        Attr attribute = doc.createAttribute("ID");
        attribute.setValue(getData[0]);
        operatingS.setAttributeNodeNS(attribute);
        
        Element name = doc.createElement("Name");
        name.appendChild(doc.createTextNode(getData[1]));
        operatingS.appendChild(name);
        
        Element developer = doc.createElement("Developer");
        developer.appendChild(doc.createTextNode(getData[2]));
        operatingS.appendChild(developer);
        
        Element osFamily = doc.createElement("OSFamily");
        osFamily.appendChild(doc.createTextNode(getData[3]));
        operatingS.appendChild(osFamily);
        
        Element latestVersion = doc.createElement("LatestVersion");
        latestVersion.appendChild(doc.createTextNode(getData[4]));
        operatingS.appendChild(latestVersion);
        
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult (new File ("C:\\Users\\Dima\\eclipse-workspace\\finalproject\\src\\finalProject\\newXML.xml"));
      
        transformer.transform(domSource, streamResult);
        
    }
    }
}