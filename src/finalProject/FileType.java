package finalProject;

import java.util.List;

import finalProject.CSV;
import finalProject.JSONfile;
import finalProject.XML;
import finalfinalproject.OperatingSystem;


public abstract class FileType {

    protected List <OperatingSystem> operatingSystems;
  
    public abstract void populateData (String content) throws Exception;
    
    public List <OperatingSystem> getOperatingSystems() {
        return this.operatingSystems;
    }
    
    public abstract void populateFile (List <String> list) throws Exception;
    
	public static FileType getInstance(String fileType) {
		
		switch(fileType) {
		case"csv":
			CSV.setDelimiter(";");
			return new CSV();
		case "json":
			return new JSONfile();
		case "xml":
			return new XML(); 
		default:
			return null;
		}
		
	}
}

