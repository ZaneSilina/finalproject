package finalProject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


public class JSONfile extends FileType {
	

		@Override
		public void populateData(String content) throws Exception {

			this.operatingSystems = new ArrayList<OperatingSystem>();
			
			JSONObject operatingSystemsObject = new JSONObject(content);
			JSONArray operatingSystemsArray = operatingSystemsObject.getJSONArray("Operating Systems");
			
			for(int i = 0; i < operatingSystemsArray.length(); i++) {
				JSONObject operatingSystemObject = operatingSystemsArray.getJSONObject(i);
				OperatingSystem operatingSystem = new OperatingSystem();
				this.operatingSystems.add(operatingSystem);
				
				operatingSystem.setId(operatingSystemObject.getInt("@ID"));
				operatingSystem.setName(operatingSystemObject.getString("Name"));
				operatingSystem.setDeveloper(operatingSystemObject.getString("Developer"));
				operatingSystem.setOsFamily(operatingSystemObject.getString("OSFamily"));
				operatingSystem.setLatestVersion(operatingSystemObject.getString("LatestVersion"));
				operatingSystem.setDelete(operatingSystemObject.getString("Delete"));
	            if (operatingSystem.getDelete().isEmpty())
	                operatingSystem.setDelete(null);
			}
		}
		
		  @Override
		    public void populateFile(List <String> list) throws Exception {
		        
			  if (!list.isEmpty()) {
		        JSONObject osDetails = new JSONObject();
		        String replaceCole = list.toString().replace(":",";");
		        String removeFirstBracket = replaceCole.replace("[", "");
		        String writeToFile = removeFirstBracket.replace("]", "");
		        String [] getData = writeToFile.split(";");
		        
		        osDetails.put("@ID", getData[0]);
		        osDetails.put("Name", getData[1]);
		        osDetails.put("Developer", getData[2]);
		        osDetails.put("OSFamily", getData[3]);
		        osDetails.put("LatestVersion", getData[4]);
		        
		        JSONObject osObject = new JSONObject();
		        osObject.put("Operating Systems", osDetails);
		        try {
		        FileWriter file = new FileWriter("C:\\Users\\Dima\\eclipse-workspace\\finalproject\\src\\finalProject\\newJSON.json");
		        file.write(osObject.toString(5));
		        file.flush();
		        }
		        catch (IOException e) {
		            e.printStackTrace();
		        }
		        
		        
		    }
		  }
}


