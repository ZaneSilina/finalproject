package openFile;

import java.util.List;

import database.Database;
import fileTypes.FileType;


public class UserChoice {
	
public void getCommand () throws Exception {
        
        Input input = new Input();
        Database db = new Database();
        String command = input.getCommandFromUser().toString();
        
        switch (command) {
        case "find": {
            FileType fileType = FileType.getInstance(input.processNew());
            List <String> list = db.getEntry(input.getIDFromConsole(),input.getNameFromConsole());
            fileType.populateFile(list);
            getCommand();
            break;  
        }
        case "update": {
            db.updateEntry(input.getIDFromConsole(), input.getUpdateFromConsole());
            getCommand();
            break;
        }
        case "delete": {
            db.deleteEntry(input.getIDToDelete());
            getCommand();
            break;
        }
        case "read": {
            db.getAllEntries();
            getCommand();
            break;
        }
        case "exit": {
            System.out.println("Programm closed.");
            break;
        }
        default: {
            System.out.println("Incorrect command - please try again!");
            getCommand();
            break;
        }
        }
        
}

}
