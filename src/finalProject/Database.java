package finalProject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Database {

    private Connection conn = null;
    
    public Database() throws Exception {
        String className = "com.mysql.cj.jdbc.Driver";
        Class.forName(className);
        this.conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "root", "");
        this.conn.setAutoCommit(false);
    }

    public void setEntries(List<OperatingSystem> operatingSystems) throws Exception {

        String sqlInsert = "insert into final_project.database_os (ID, Name, Developer, OSFamily, LatestVersion) values (?, ?, ?, ?, ?)";
        PreparedStatement statement = this.conn.prepareStatement(sqlInsert);

        Iterator<OperatingSystem> iterator = operatingSystems.iterator();

        while (iterator.hasNext()) {
            OperatingSystem currentOS = iterator.next();
            statement.setInt(1, currentOS.getId());
            statement.setString(2, currentOS.getName());
            statement.setString(3, currentOS.getDeveloper());
            statement.setString(4, currentOS.getOsFamily());
            statement.setString(5, currentOS.getLatestVersion());
           
         
            if (currentOS.getDelete() == null)
            statement.addBatch();
        }
    
        int []count = statement.executeBatch();

        boolean success = false;
        for (int countItem : count) {
            if (countItem == 0)
                System.out.println("Some entries are not updated");
            else 
               success = true;
        }
    
        if(success) {
            this.conn.commit();
            System.out.println("Entries are updated");
        }

    }
    
  
    public List<String> getEntry(String id, String name) throws IOException {
    	List<String> returnedList = new ArrayList<String>();
    	OperatingSystem os = new OperatingSystem();
    	
    	try {
    		String sql = "SELECT * FROM final_project.database_os WHERE ID LIKE ? and Name LIKE ?";
    		PreparedStatement preparedStatement = this.conn.prepareStatement(sql);
    		preparedStatement.setString(1, id);
    		preparedStatement.setString(2, "%" + name + "%");
    		ResultSet rs = preparedStatement.executeQuery();
    			
    		if(!(rs.next()))
    			System.out.println("No match");
    		else {	
    			 int osId = rs.getInt(1);
                 String osName = rs.getString(2);
                 String osDeveloper = rs.getString(3);
                 String osFamily = rs.getString(4);
                 String osLatestVersion = rs.getString(5);
                 
                 returnedList.add(osId + ":" + osName + ":" + osDeveloper + ":" + osFamily + ":" + osLatestVersion);
                 System.out.println("Required entry is added to file.");
    		}	
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
    	return returnedList;
    }
    
    public void getAllEntries () {
    
		try {
			String sql = "SELECT * FROM final_project.database_os";
			PreparedStatement statement = this.conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			
			while (rs.next())				
				System.out.println(rs.getInt(1) + ":" + rs.getString(2) + ":" + rs.getString(3) + ":" + rs.getString(4) + ":" + rs.getString(5));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    public void updateEntry(String id, String latestVersion) {
    	try {
    		String sql = "UPDATE final_project.database_os SET LatestVersion = ?  WHERE ID = ?";
    		PreparedStatement updateStatement = this.conn.prepareStatement(sql);
    		updateStatement.setString(2, id);
    		updateStatement.setString(1, latestVersion);
    		int count = updateStatement.executeUpdate();
    		
    		if(count > 0) {
				conn.commit();
				System.out.println("Entry updated");
			} else
				System.out.println("Entry is not updated");
    	} catch (SQLException e) {
			e.printStackTrace();
    }
   }
    
    
    public void deleteEntry(String string) {
    	try {
			String sql= "DELETE FROM final_project.database_os WHERE id = ?";
			PreparedStatement deleteStatement = conn.prepareStatement(sql);
			deleteStatement.setString(1, string);
			int count = deleteStatement.executeUpdate();
			
			if(count > 0) {
				conn.commit();
				System.out.println("Entry is deleted");
			} else {
				conn.rollback();
				System.out.println("Entry is not deleted");
		
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    public void closeConnecion() {
			try {
				if(conn != null)
					conn.close();
				conn = null;
				System.out.println("Connection is closed");
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	
}
