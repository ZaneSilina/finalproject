package finalProject;

import java.util.List;

import database.Database;
 
import fileTypes.FileType;
import openFile.FileReader;
import openFile.Input;
import openFile.UsersChoice;

public class Run {
	
    public static void main(String[] args) throws Exception {
        Input input = new Input();
        input.process();
        
        FileReader fileReader = new FileReader();
        fileReader.setFile(input.getFilePath());
        
        
        FileType fileType = FileType.getInstance(input.getFileExtension());
        fileType.populateData(fileReader.getContent());
        Database db = new Database();
        db.setEntries(fileType.getOperatingSystems());
        
        UsersChoice choice = new UsersChoice();
        choice.getCommand();
		
		db.closeConnecion();
        
    }
}
